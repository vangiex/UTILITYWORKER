var json1 = [
{"company_name":"project A","present_worth":"8"},
{"company_name":"project B","present_worth":"6"},
{"company_name":"project c","present_worth":"9"}
];

var json2 = [
{"company_name":"project A","present_worth":"81"},
{"company_name":"project B","present_worth":"63"},
{"company_name":"project c","present_worth":"92"}
];

var json3 = [
{"company_name":"project A","present_worth":"80"},
{"company_name":"project B","present_worth":"69"},
{"company_name":"project c","present_worth":"98"}
];

var json4 = [
{"company_name":"project A","present_worth":"83"},
{"company_name":"project B","present_worth":"34"},
{"company_name":"project c","present_worth":"25"}
];

var chartjsData1 = [];
var chartjsData2 = [];
var chartjsData3 = [];
var chartjsData4 = [];

for (var i = 0; i < json1.length; i++) 
{
    chartjsData1.push(json1[i].present_worth);
}

for (var i = 0; i < json2.length; i++) 
{
    chartjsData2.push(json2[i].present_worth);
}

for (var i = 0; i < json3.length; i++) 
{
    chartjsData3.push(json3[i].present_worth);
}

for (var i = 0; i < json4.length; i++) 
{
    chartjsData4.push(json4[i].present_worth);
}

var datasetconfig1 = [
						{
							label: 'CS',
							backgroundColor: '#ff6384',
							borderColor: '#ff6384',
							data : chartjsData1,
							fill: false
						}
					];
					
var datasetconfig2 = [
						{
							label: 'EC',
							backgroundColor: '#ff6322',
							borderColor: '#ff6322',
							data : chartjsData2,
							fill: false
						}
					];
					
var datasetconfig3 = [
						{
							label: 'CIVIL',
							backgroundColor: '#52be80',
							borderColor: '#52be80',
							data : chartjsData3,
							fill: false
						}
					];
var datasetconfig4 = [
						{
							label: 'MECH',
							backgroundColor: '#800000',
							borderColor: '#800000',
							data : chartjsData4,
							fill: false
						}
					];
					
var labelconfig = ["Lecture 1","Lecture 2","Lecture 3","Lecture 4","Lecture 5","Lecture 6","Lecture 7","Lecture 8","Lecture 9","Lecture 10"];

var ChartData1 = { labels : labelconfig , datasets : datasetconfig1 };
var canvas1 = document.getElementById("canvas1");
var chartconfig1 = { type: 'line', data: ChartData1 };
var Chart1 = new Chart( canvas1, chartconfig1 );

var ChartData2 = { labels : labelconfig , datasets : datasetconfig2 };
var canvas2 = document.getElementById("canvas2");
var chartconfig2 = { type: 'line', data: ChartData2 };
var Chart2 = new Chart( canvas2, chartconfig2 );

var ChartData3 = { labels : labelconfig , datasets : datasetconfig3 };
var canvas3 = document.getElementById("canvas3");
var chartconfig3 = { type: 'line', data: ChartData3 };
var Chart3 = new Chart( canvas3, chartconfig3 );

var ChartData4 = { labels : labelconfig , datasets : datasetconfig4 };
var canvas4 = document.getElementById("canvas4");
var chartconfig4 = { type: 'line', data: ChartData4 };
var Chart4 = new Chart( canvas4, chartconfig4 );    

